module Spree
	Spree::HomeController.class_eval do
		respond_to :html

		def index
			# fresh_when etag: store_etag, last_modified: store_last_modified, public: true
			@products = Product.all
		end

		def shops
      @vendors = Vendor.all
    end

    def shop_products
      @vendor = Vendor.find(params[:id])
      # @taxonomies = @vendor.taxonomies
      @products = @vendor.products
    end
	end
end